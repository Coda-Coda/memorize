module.exports = {
    env: {
        node: true,
        browser: true,
        jest: true,
        es6: true,
    },
    parser: '@typescript-eslint/parser',
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'prettier/@typescript-eslint',
        'plugin:prettier/recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
    ],
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    rules: {
        'no-console': ['error'],
        'no-useless-escape': ['warn'],
        'import/no-extraneous-dependencies': ['error'],
    },
};