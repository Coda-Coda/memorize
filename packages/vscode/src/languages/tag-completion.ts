import {
  CompletionItem,
  SnippetString,
  CompletionItemProvider,
  workspace,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { loadZettelkasten } from '@memorize/file-system';

export const tagCompletionProvider: CompletionItemProvider = {
  async provideCompletionItems() {
    const rootPath = workspace.rootPath;
    if (!rootPath) {
      return [];
    }
    const zettelkasten = await loadZettelkasten(rootPath);
    return zettelkasten.meta.tags.map(
      (h): CompletionItem => {
        const idCompletion = new CompletionItem(`#${h}`);
        idCompletion.insertText = new SnippetString(h);
        return idCompletion;
      },
    );
  },
};
