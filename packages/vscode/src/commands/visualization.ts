import {
  window,
  workspace,
  WebviewPanel,
  ViewColumn,
  ExtensionContext,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { loadZettelkasten } from '@memorize/file-system';
import { getZettelkastenVisualization } from '@memorize/visualization';
import { ZettelkastenVisualizationEnum } from '@memorize/types';

async function getZettelkastenCanvas(rootPath: string): Promise<string> {
  const zettelkasten = await loadZettelkasten(rootPath);
  const canvas = getZettelkastenVisualization(
    zettelkasten,
    ZettelkastenVisualizationEnum.FORCE,
  );
  return canvas.toSVG();
}

function getWebviewContent(): string {
  return `<!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
      <script>
        window.addEventListener('message', (event) => {
          if (!event || !event.data) {
            return;
          }
          const data = event.data;
          document.body.innerHTML = data;
        });
        const vscode = acquireVsCodeApi();
        vscode.postMessage({
          command: 'load',
        });
      </script>
  </body>
  </html>`;
}

export async function visualizeZettelkasten(
  context: ExtensionContext,
  currentPanel: WebviewPanel | undefined,
): Promise<void> {
  const rootPath = workspace.rootPath;
  if (!rootPath) {
    return;
  }

  const columnToShowIn = window.activeTextEditor
    ? window.activeTextEditor.viewColumn
      ? window.activeTextEditor.viewColumn
      : ViewColumn.Beside
    : ViewColumn.Beside;

  if (currentPanel) {
    // If we already have a panel, show it in the target column
    currentPanel.reveal(columnToShowIn);
    return;
  }

  // Create and show a new webview
  currentPanel = window.createWebviewPanel(
    'visualization', // Identifies the type of the webview. Used internally
    `Zettelkasten visualization - ${rootPath}`, // Title of the panel displayed to the user
    columnToShowIn, // Editor column to show the new webview panel in.
    {
      enableScripts: true,
    }, // Webview options. More on these later.
  );

  currentPanel.webview.onDidReceiveMessage(
    async (message) => {
      if (message.command === 'load') {
        if (!currentPanel) {
          return;
        }

        const canvas = await getZettelkastenCanvas(rootPath);
        currentPanel.webview.postMessage(canvas);

        const interval = setInterval(async () => {
          if (!currentPanel) {
            interval.unref();
            return;
          }

          const canvas = await getZettelkastenCanvas(rootPath);
          currentPanel.webview.postMessage(canvas);
        }, 10000);
        return;
      }

      throw new Error('visualization(visualizeZettelkasten): unknown command');
    },
    undefined,
    context.subscriptions,
  );

  currentPanel.webview.html = getWebviewContent();

  currentPanel.onDidDispose(
    () => {
      // When the panel is closed, cancel any future updates to the webview content
      currentPanel = undefined;
    },
    null,
    context.subscriptions,
  );
}
