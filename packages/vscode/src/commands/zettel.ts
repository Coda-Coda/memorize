import {
  window,
  workspace,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { id, sanitizeText } from '@memorize/core';
import { newZettel } from '@memorize/file-system';
import { showInputBox } from './vscode-utils';

export function generateId(): void {
  const editor = window.activeTextEditor;

  if (!editor) {
    return;
  }

  editor.edit((e) => e.insert(editor.selection.active, id(new Date())));
}

export async function createNewZettel(): Promise<void> {
  const root = workspace.rootPath;
  const path = await showInputBox(
    'Enter path',
    'Path to you zettelkasten directory',
    root,
  );
  if (!path) {
    throw new Error('New zettel: you must provide a path');
  }

  const title = await showInputBox(
    'Enter the title of your new zettel',
    'Your new zettel title',
  );
  if (!title) {
    throw new Error('New zettel: you must provide a title');
  }

  const filename = await showInputBox(
    'Enter filename',
    'The filename for your new zettel',
    sanitizeText(title),
  );
  if (!filename) {
    throw new Error('New zettel: you must provide a filename');
  }

  await newZettel({
    id: id(new Date()),
    metadata: {
      filename,
      path,
    },
    references: [],
    tags: [],
    title,
  });
}
