# Memorize - VSCode

Hi there :smile:

This package is the Visual Studio Code extension of Memorize. 

:notebook: Memorize is a set of tools that allows you to use markdown files as a Zettelkasten note-taking system.

## Features

Current features :

- [X] Generate new Id
- [X] Display graph visualization
- [X] Link autocompletion
- [X] Tag autocompletion
- [X] Link zettel from Id
- [X] Create new zettel

Future features :

- [ ] Interactive graph visualization
- [ ] Tag visualization

## Commands

- Memorize: New zettel ➡️ Create new zettel
- Memorize: Generate id ➡️ Generate id on the cursor position
- Memorize: Visualize ➡️ Display graph visualization of the zettelkasten

## Requirements

At this moment there is nos pecific requirements other than VSCode.

## Extension Settings

There are no particular extension settings.
