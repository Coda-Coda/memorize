import { jaroDistance } from './jaro';
import { examples } from './examples';

describe('Testing Jaro distance', () => {
  describe('When I provide the same string', () => {
    test('It should return 1', () => {
      expect(jaroDistance('jaro-winler', 'jaro-winler')).toEqual(1);
    });
    describe('And one of the stirng contains the other', () => {
      test('It should NOT return 1', () => {
        expect(jaroDistance('jaro', 'jaro-winler')).not.toEqual(0);
      });
    });
  });

  describe('When I provide two empty strings', () => {
    test('It should return 1', () => {
      expect(jaroDistance('', '')).toEqual(1);
    });
  });

  describe('When I provide an empty string as first parameter', () => {
    test('It should return 0', () => {
      expect(jaroDistance('', 'jaro-winler')).toEqual(0);
    });
  });

  describe('When I provide an empty string as second parameter', () => {
    test('It should return 0', () => {
      expect(jaroDistance('jaro-winkler', '')).toEqual(0);
    });
  });

  describe('When I test the names used on the original study', () => {
    examples.forEach(({ s1, s2, jaro }) => {
      test(`'${s1}', '${s2}', should return '${jaro}'`, () => {
        expect(jaroDistance(s1, s2)).toEqual(jaro);
      });
    });
  });
});
