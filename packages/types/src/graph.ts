const ARC = 'ARC';
const EDGE = 'EDGE';
const FORCE = 'FORCE';

export type ZettelkastenVisualizationType =
  | typeof ARC
  | typeof EDGE
  | typeof FORCE;

export const ZettelkastenVisualizationEnum: {
  [key: string]: ZettelkastenVisualizationType;
} = {
  ARC,
  EDGE,
  FORCE,
};

export const ZettelkastenVisualizationArray = Object.getOwnPropertyNames(
  ZettelkastenVisualizationEnum,
);
