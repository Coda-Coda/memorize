import { ZettelInterface } from './zettel';

export interface ZettelkastenInterface {
  meta: {
    updatedAt: number;
    tags: string[];
  };
  zettels: ZettelInterface[];
}
