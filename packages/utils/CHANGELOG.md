# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/utils





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)

**Note:** Version bump only for package @memorize/utils





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)

**Note:** Version bump only for package @memorize/utils





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **utils:** add new utils module ([ebb2bbb](https://gitlab.com/memorize_it/memorize/commit/ebb2bbb6403433c697c1c3487e0bf3838d21624f))
