import { sanitizeText } from '@memorize/core';
import { Question, ListQuestion, InputQuestion } from 'inquirer';
import { ZettelkastenVisualizationArray } from '@memorize/types';

const nameQuestion: InputQuestion = {
  message: 'Enter the name ...',
  name: 'name',
  type: 'input',
};

function sanitizeTags(tags: string): string[] {
  return tags.length ? tags.split(',').map((t) => sanitizeText(t)) : [];
}

const tagsQuestion: InputQuestion = {
  message: 'Enter the tags ... (separated by comma ",")',
  name: 'tags',
  type: 'input',
  filter: sanitizeTags,
};

function pathQuestion(): InputQuestion {
  const currentPath = process.cwd();
  return {
    message: `Enter the path ... (${currentPath})`,
    name: 'path',
    type: 'input',
    default: currentPath,
  };
}

function filenameQuestion(title: string): InputQuestion {
  const defaultFilename = sanitizeText(title);
  return {
    message: `Enter the filename ... (${defaultFilename})`,
    name: 'filename',
    type: 'input',
    default: defaultFilename,
  };
}

export function pathQuestions(): Question[] {
  return [pathQuestion()];
}

export const zettelNameQuestions: Question[] = [nameQuestion];

export function zettelDetailsQuestions(title: string): Question[] {
  return [filenameQuestion(title), tagsQuestion];
}

const diagramTypeQuestion: ListQuestion = {
  message: 'Choose the diagram type...',
  name: 'type',
  type: 'list',
  choices: ZettelkastenVisualizationArray,
};

export function diagramTypeQuestions(): Question[] {
  return [diagramTypeQuestion];
}

function validateTagQuestion(
  tag: string,
  tagCandidates: string[],
): ListQuestion {
  return {
    message: `When you used ${tag}, are you sure you didn't mean...`,
    name: 'tag',
    type: 'list',
    choices: [tag, ...tagCandidates],
  };
}

export function validateTagQuestions(
  tag: string,
  tagCandidates: string[],
): Question[] {
  return [validateTagQuestion(tag, tagCandidates)];
}
