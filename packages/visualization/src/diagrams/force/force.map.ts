import { ZettelkastenInterface } from '@memorize/types';
import { GraphInterface, LinkInterface } from '../../types/graph';

export function zettelkastenToForceDiagramGraph({
  zettels,
}: ZettelkastenInterface): GraphInterface {
  const idToIndexMap = zettels.reduce(
    (map: { [key: string]: number }, z, i) => {
      map[z.id] = i;
      return map;
    },
    {},
  );

  return zettels.reduce(
    (g: GraphInterface, { id, title, references }, index) => {
      g.nodes.push({
        index,
        id,
        title,
        group: 1,
      });

      g.links.push(
        ...references.map(
          (r: string): LinkInterface => ({
            source: index,
            target: idToIndexMap[r],
            value: 1,
          }),
        ),
      );

      return g;
    },
    {
      nodes: [],
      links: [],
    },
  );
}
