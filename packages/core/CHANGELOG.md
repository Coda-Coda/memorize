# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/core





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **core:** add method to zettelkasten to consolidate tags ([9ad3e12](https://gitlab.com/memorize_it/memorize/commit/9ad3e129f9502ff7baa7d0920f53ca5b5e1f8ef3))





## [0.3.1](https://gitlab.com/memorize_it/memorize/compare/v0.3.0...v0.3.1) (2020-02-08)


### Bug Fixes

* **types:** use @memorize/types as dependency to allow use of enums,maps... on runtime ([58c64b0](https://gitlab.com/memorize_it/memorize/commit/58c64b06160c105ac9073e82de8b4d087883fe68))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Bug Fixes

* **core:** move regeps from types to core. They are used on the runtime !!! XD ([c613128](https://gitlab.com/memorize_it/memorize/commit/c6131288b5c50d6962e8c59ee6f4df03a4d4c60d))


### Features

* **core:** allow to search for similar  tags already exisitng ([984c478](https://gitlab.com/memorize_it/memorize/commit/984c47881fa4a32499fcb84f31bc07d44e03d7d3))
* **core:** wHen parsing similartags don't make any suggestion if tag already exists ([ae843b9](https://gitlab.com/memorize_it/memorize/commit/ae843b97e913167e99e14dc5d8a6cf853d14de16))





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **graph:** zettelkasten to graph funtion ([41df447](https://gitlab.com/memorize_it/memorize/commit/41df447ce26cd8d4220bd5815a40c1054e21edf9))
* **zettel:** add sanitize  text method ([fb0cfcf](https://gitlab.com/memorize_it/memorize/commit/fb0cfcf7c35a342d4494c24adf89da79038217ec))





## [0.1.1](https://gitlab.com/memorize_it/memorize/compare/v0.1.0...v0.1.1) (2020-01-18)

**Note:** Version bump only for package @memorize/core





# 0.1.0 (2020-01-18)


### Features

* **cli:** add cli with create id feature ([02fa676](https://gitlab.com/memorize_it/memorize/commit/02fa67638c6745cea73d2026e187b9b9980102bc))
* **core:** create id from date ([85d59c2](https://gitlab.com/memorize_it/memorize/commit/85d59c2a74173d1c3ed224c9e9ee1b7295d3ed4c))
