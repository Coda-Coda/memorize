export const IDRegexp = /\d{14}/gm;
export const TagRegexp = /(\#[a-zA-Z0-9_\-]+)/gm;

export function sanitizeText(text: string): string {
  if (!text) {
    throw new Error('zettel(sanitizeText): empty string provided');
  }

  return text
    .toLocaleLowerCase()
    .replace(/[^a-zA-Z0-9\ ]/g, '')
    .trim()
    .replace(/\ /g, '_');
}
