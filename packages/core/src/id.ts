import { format } from 'date-fns';

function id(date: string | Date): string {
  if (typeof date === 'string') {
    date = new Date(date);
  }

  return format(date, 'yyyyMMddHHmmss');
}

export { id };
